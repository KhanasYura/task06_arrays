package com.khanas.Container;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ViewContainer {
    private Container container;
    private static Logger logger = LogManager.getLogger();
    private static Scanner sc = new Scanner(System.in);

    public ViewContainer() {
        container = new Container();
        while (true) {
            logger.info("1.Add element");
            logger.info("2.Get element");
            logger.info("3.Set element");
            logger.info("4.Show elements");
            logger.info("5.Exit");
            logger.info("Enter: ");
            int index = sc.nextInt();
            if (index == 1) {
                logger.info("Enter string:");
                sc.nextLine();
                String str = sc.nextLine();
                container.add(str);
            } else if (index == 2) {
                logger.info("Enter index:");
                int getIndex = sc.nextInt();
                logger.info(container.get(getIndex));
            } else if (index == 3) {
                logger.info("Enter index:");
                int getIndex = sc.nextInt();
                logger.info("Enter string:");
                sc.nextLine();
                String str = sc.nextLine();
                container.set(getIndex, str);
            } else if (index == 4) {
                logger.info(container.toString());
            } else if (index == 5) {
                break;
            } else {
                logger.error("Wrong input");
            }
        }
    }

}
