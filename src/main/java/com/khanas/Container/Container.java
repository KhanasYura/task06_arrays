package com.khanas.Container;

public class Container {
    private String[] arr;
    private int size;
    private int index = 0;

    public Container() {
        this.arr = new String[5];
        this.size = 5;
    }

    public final void add(final String str) {
        System.out.println(size);
        System.out.println(index);
        if (size <= index) {
            String[] arrBigger = new String[size + 5];
            size += 5;
            System.arraycopy(arr, 0, arrBigger, 0, arr.length);
            arr = new String[size + 5];
            System.arraycopy(arrBigger, 0, arr, 0, arrBigger.length);
        }
        arr[index] = str;
        index += 1;
    }

    public final String get(final int value) {
        if (value >= index) {
            return "Wrong index";
        }
        return arr[value];
    }

    public final void set(final int value, final String str) {
        if (value < index) {
            arr[value] = str;
        }
    }

}
