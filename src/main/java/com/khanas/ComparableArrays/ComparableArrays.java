package com.khanas.ComparableArrays;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class ComparableArrays {
    private Scanner sc = new Scanner(System.in);
    private Logger logger = LogManager.getLogger(ComparableArrays.class);
    private Person[] arr;
    private ArrayList<Person> arrList = new ArrayList<>();

    public ComparableArrays() {
        while (true) {
            logger.info("1.Add to array");
            logger.info("2.Binary search");
            logger.info("3.Comparable and Comparator");
            logger.info("4.Exit");
            logger.info("Enter: ");
            int index = sc.nextInt();
            if (index == 1) {
                logger.info("Enter name:");
                sc.nextLine();
                String name = sc.nextLine();
                logger.info("Enter surname:");
                String surname = sc.nextLine();
                arrList.add(new Person(name, surname));
                arr = new Person[arrList.size()];
                for (int i = 0; i < arrList.size(); i++) {
                    arr[i] = arrList.get(i);
                }
            } else if (index == 2) {
                logger.info("Enter surname to search: ");
                sc.nextLine();
                String surname = sc.nextLine();
                int index1 = binarySearchBySurname(surname);
                if (index1 == -1) {
                    logger.info(surname + " is not found.");
                } else {
                    logger.info(surname + " is on " + index1 + " index.");
                }
            } else if (index == 3) {
                logger.info("------------Sort Array by Name(Comparable)--"
                        + "----------");
                sortArray();
                logger.info("------------Sort ArrayList by Name(Comparable)--"
                        + "----------");
                sortArrayList();
                Comparator<Person> comparatorByName = Comparator.comparing(obj
                        -> obj.getName());
                Comparator<Person> comparatorBySurname = Comparator.comparing(obj
                        -> obj.getSurname());
                logger.info("------------Sort Array by Name(Comparator)----"
                        + "--------");
                sortArray(comparatorByName);
                logger.info("------------Sort Array by Surname(Comparator)--"
                        + "----------");
                sortArray(comparatorBySurname);
                logger.info("------------Sort ArrayList by Name(Comparator)--"
                        + "----------");
                sortArrayList(comparatorByName);
                logger.info("------------Sort ArrayList by Surname(Comparator)"
                        + "------------");
                sortArrayList(comparatorBySurname);
            } else if (index == 4) {
                break;
            } else {
                logger.error("Wrong index.");
            }
        }
    }

    class PersonNameComparator implements Comparator<String> {

        public int compare(String a, String b) {

            return a.compareTo(b);
        }
    }

    public final int binarySearchBySurname(String surname) {
        Comparator comparator = new PersonNameComparator();
        int low = 0;
        int high = arrList.size() - 1;
        int mid;
        while (low <= high) {
            mid = (low + high) / 2;
            if (comparator.compare(arrList.get(mid).getSurname(),
                    surname) < 0) {
                low = mid + 1;
            } else if (comparator.compare(arrList.get(mid).getSurname(),
                    surname) > 0) {
                high = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }


    public final void sortArray(Comparator comparator) {
        logger.info("Before");
        for (int i = 0; i < arrList.size(); i++) {
            logger.info(arr[i].getName() + " "
                    + arr[i].getSurname());
        }
        logger.info("");
        Arrays.sort(arr, comparator);
        logger.info("After");
        for (int i = 0; i < arrList.size(); i++) {
            logger.info(arr[i].getName() + " "
                    + arr[i].getSurname());
        }
        logger.info("");
    }


    public final void sortArrayList(Comparator comparator) {
        logger.info("Before");
        for (int i = 0; i < arrList.size(); i++) {
            logger.info(arrList.get(i).getName()
                    + " " + arrList.get(i).getSurname());
        }
        logger.info("");
        Collections.sort(arrList, comparator);
        logger.info("After");
        for (int i = 0; i < arrList.size(); i++) {
            logger.info(arrList.get(i).getName()
                    + " " + arrList.get(i).getSurname());
        }
        logger.info("");
    }


    public final void sortArray() {
        logger.info("Before");
        for (int i = 0; i < arrList.size(); i++) {
            logger.info(arr[i].getName() + " "
                    + arr[i].getSurname());
        }
        logger.info("");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i].compareTo(arr[j]) < 0) {
                    Person temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        logger.info("After");
        for (int i = 0; i < arrList.size(); i++) {
            logger.info(arr[i].getName() + " "
                    + arr[i].getSurname());
        }
    }

    public final void sortArrayList() {
        logger.info("Before");
        for (int i = 0; i < arrList.size(); i++) {
            logger.info(arrList.get(i).getName()
                    + " " + arrList.get(i).getSurname());
        }
        logger.info("");
        for (int i = 0; i < arrList.size(); i++) {
            for (int j = 0; j < arrList.size(); j++) {
                if (arrList.get(i).compareTo(arrList.get(j)) < 0) {
                    Person temp = arrList.get(i);
                    arrList.set(i, arrList.get(j));
                    arrList.set(j, temp);
                }
            }
        }
        logger.info("After");
        for (int i = 0; i < arrList.size(); i++) {
            logger.info(arrList.get(i).getName() + " " + arrList.get(i).getSurname());
        }
    }
}
