package com.khanas.ComparableArrays;

public class Person implements Comparable<Person> {

    private String name;
    private String surname;

    Person(final String name1, final String surname1) {

        this.name = name1;
        this.surname = surname1;
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name1) {
        this.name = name1;
    }

    public final String getSurname() {
        return surname;
    }

    public final void setSurname(final String surname1) {
        this.surname = surname1;
    }

    public final int compareTo(final Person p) {
        return name.compareTo(p.getName());
    }

}
