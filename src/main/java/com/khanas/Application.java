package com.khanas;

import com.khanas.ComparableArrays.ComparableArrays;
import com.khanas.Container.ViewContainer;
import com.khanas.Deque.ViewDeque;
import com.khanas.Generic.ViewGeneric;
import com.khanas.LogicalTasks.LogicalTasks;
import com.khanas.PriorityQueue.ViewPriorityQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Application {

    private static Logger logger = LogManager.getLogger();
    private static Scanner sc = new Scanner(System.in);

    private Application() {
    }

    public static void main(final String[] args) {
        while (true) {
            logger.info("1.PriorityQueue");
            logger.info("2.Deque");
            logger.info("3.Container");
            logger.info("4.Generic");
            logger.info("5.Comparable arrays");
            logger.info("6.Logical tasks");
            logger.info("7.Exit");
            logger.info("Enter: ");
            int index = sc.nextInt();
            if (index == 1) {
                new ViewPriorityQueue();
            } else if (index == 2) {
                new ViewDeque();
            } else if (index == 3) {
                new ViewContainer();
            } else if (index == 4) {
                new ViewGeneric();
            } else if (index == 5) {
                new ComparableArrays();
            } else if (index == 6) {
                new LogicalTasks();
            } else if (index == 7) {
                break;
            } else {
                logger.error("Wrong index.");
            }
        }
    }


}
