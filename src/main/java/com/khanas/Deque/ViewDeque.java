package com.khanas.Deque;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.Scanner;

public class ViewDeque {
    private static Logger logger = LogManager.getLogger();
    private static Scanner sc = new Scanner(System.in);
    private Deque<String> deque;

    public ViewDeque() {
        deque = new Deque<>();
        while (true) {
            logger.info("1.Add first element");
            logger.info("2.Push element");
            logger.info("3.Get size");
            logger.info("4.Get element");
            logger.info("5.Show elements");
            logger.info("6.Remove element");
            logger.info("7.Exit");
            logger.info("Enter: ");
            int index = sc.nextInt();
            if (index == 1) {
                logger.info("Enter string:");
                sc.nextLine();
                String str = sc.nextLine();
                deque.addFirst(str);
            } else if (index == 2) {
                logger.info("Enter string:");
                sc.nextLine();
                String str = sc.nextLine();
                deque.push(str);
            } else if (index == 3) {
                logger.info("Size: " + deque.size());
            } else if (index == 4) {
                logger.info("Enter index:");
                int getIndex = sc.nextInt();
                if (deque.get(getIndex) == null) {
                    logger.error("Wrong index");
                } else {
                    logger.info(deque.get(getIndex));
                }
            } else if (index == 5) {
                Iterator iterator = deque.iterator();
                while (iterator.hasNext()) {
                    logger.info(iterator.next());
                }
            } else if (index == 6) {
                logger.info("Enter index:");
                int getIndex = sc.nextInt();
                deque.remove(getIndex);
            } else if (index == 7) {
                break;
            } else {
                logger.error("Wrong input");
            }
        }
    }
}
