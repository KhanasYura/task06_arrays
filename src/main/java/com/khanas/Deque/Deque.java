package com.khanas.Deque;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;

public class Deque<T> {
    private static Logger logger = LogManager.getLogger();

    private class Node {
        private T date;
        private Node next;
        private Node previous;

        public Node getPrevious() {
            return previous;
        }

        public void setPrevious(Node previous) {
            this.previous = previous;
        }

        private void setDate(T date) {
            this.date = date;
        }

        private void setNext(Node next) {
            this.next = next;
        }

        private T getDate() {
            return date;
        }

        private Node getNext() {
            return next;
        }

    }

    public Node root;
    public Node tail;

    public Deque() {
        root = new Node();
        tail = root;
    }

    public void addFirst(T date) {
        if (root.getDate() == null) {
            root = new Node();
            root.setDate(date);
            tail = root;
        } else {
            Node addfirst = new Node();
            addfirst.setNext(root);
            addfirst.setDate(date);
            root.setPrevious(addfirst);
            root = addfirst;
        }
    }

    public void push(T date) {
        if (tail.getDate() == null) {
            root.setDate(date);
            tail = root;
        } else {
            Node push = new Node();
            tail.setNext(push);
            push.setPrevious(tail);
            push.setDate(date);
            tail = push;
        }
    }

    public T get(int index) {
        Node result = root;
        for (int i = 0; i <= index; i++) {
            if (result.getNext() == null) {
                return null;
            } else {
                result = result.getNext();
            }
        }
        return result.getDate();
    }

    public void clear() {
        root = new Node();
        tail = root;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Node result = root;
        while (result != null) {
            sb.append(result.getDate()).append(" ");
            result = result.getNext();
        }
        return new String(sb);
    }


    public int size() {
        Node result = root;
        int count = 1;
        if (root.getNext() == null) {
            return 0;
        }
        while (result.getNext() != null) {
            count++;
            result = result.getNext();
        }
        return count;
    }

    public void removeFirst() {
        remove(0);
    }

    public void remove(int index) {
        Node result = root;
        int i = 0;
        Node prev = null;
        int check = 0;
        while (result != null) {
            if (index == i) {
                check = 1;
                if (prev == null) {
                    root = result.getNext();
                } else if (result.getNext() == null) {
                    prev.setNext(null);
                } else {
                    prev.setNext(result.getNext());
                }
                break;
            }
            prev = result;
            result = result.getNext();
            i++;
        }
        if (check == 0) {
            logger.info("Wrong index");
        }
    }


    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int current;

            {
                current = -1;
            }

            @Override
            public boolean hasNext() {
                return current < size() - 1;
            }

            @Override
            public T next() {
                return get(current++);
            }
        };
    }

}
