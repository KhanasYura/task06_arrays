package com.khanas.PriorityQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.Scanner;

public class ViewPriorityQueue {
    private static Logger logger = LogManager.getLogger();
    private static Scanner sc = new Scanner(System.in);
    private PQueue<String> priorityQueue;

    public ViewPriorityQueue() {
        priorityQueue = new PQueue<>();
        while (true) {
            logger.info("1.Add element");
            logger.info("2.Get element");
            logger.info("3.Get size");
            logger.info("4.Remove element");
            logger.info("5.Show elements");
            logger.info("6.Exit");
            logger.info("Enter: ");
            int index = sc.nextInt();
            if (index == 1) {
                logger.info("Enter string:");
                sc.nextLine();
                String str = sc.nextLine();
                priorityQueue.add(str);
            } else if (index == 2) {
                logger.info("Enter index:");
                int getIndex = sc.nextInt();
                if (priorityQueue.get(getIndex) == null) {
                    logger.error("Wrong index");
                } else {
                    logger.info(priorityQueue.get(getIndex));
                }
            } else if (index == 3) {
                logger.info("Size: " + priorityQueue.size());
            } else if (index == 4) {
                logger.info("Enter index:");
                int getIndex = sc.nextInt();
                priorityQueue.remove(getIndex);
            } else if (index == 5) {
                Iterator iterator = priorityQueue.iterator();
                while (iterator.hasNext()) {
                    logger.info(iterator.next());
                }
            } else if (index == 6) {
                break;
            } else {
                logger.error("Wrong input");
            }
        }
    }
}
