package com.khanas.PriorityQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Iterator;

public class PQueue<T> {
    private static Logger logger = LogManager.getLogger();
    Object[] pQueue;
    int size;
    int index = 0;


    public PQueue() {
        pQueue = new Object[5];
        size = 5;
    }

    public int size() {
        return index;
    }

    public boolean add(T t) {
        if (t == null)
            throw new NullPointerException();

        if (index >= size)
            grow(index + 5);
        if (index == 0)
            pQueue[0] = t;
        else
            siftUpComparable(index, t);
        index++;
        return true;
    }

    private void grow(int minCapacity) {
        Object[] arrBigger = new Object[size + 5];
        size += 5;
        System.arraycopy(pQueue, 0, arrBigger, 0, pQueue.length);
        pQueue = new Object[size + 5];
        System.arraycopy(arrBigger, 0, pQueue, 0, arrBigger.length);
    }

    public T get(int index1) {
        if (index1 >= index && index1 < 0) {
            logger.error("Wrong index");
            return null;
        }
        return (T) pQueue[index1];
    }


    public boolean remove(int index1) {
        if (index1 <= index && index1 >= 0) {
            Object[] arr = new Object[size];
            int count = 0;
            for (int i = 0; i < pQueue.length; i++) {
                if (i != index1) {
                    arr[i - count] = pQueue[i];
                } else {
                    count++;
                }
            }
            index--;
            pQueue = new Object[size];
            System.arraycopy(arr, 0, pQueue, 0, arr.length);
            return true;
        } else {
            return false;
        }
    }


    private T removeAt(int i) {
        int s = --index;
        index--;
        if (s == i)
            pQueue[i] = null;
        else {
            T moved = (T) pQueue[s];
            pQueue[s] = null;
            siftDownComparable(i, moved);
            if (pQueue[i] == moved) {
                siftUpComparable(i, moved);
                if (pQueue[i] != moved)
                    return moved;
            }
        }
        return null;
    }


    public void siftUpComparable(int k, T x) {
        Comparable<? super T> key = (Comparable<? super T>) x;
        while (k > 0) {
            int parent = (k - 1) >>> 1;
            Object e = pQueue[parent];
            if (key.compareTo((T) e) >= 0)
                break;
            pQueue[k] = e;
            k = parent;
        }
        pQueue[k] = key;
    }

    private void siftDownComparable(int k, T x) {
        Comparable<? super T> key = (Comparable<? super T>) x;
        int half = size >>> 1;
        while (k < half) {
            int child = (k << 1) + 1;
            Object c = pQueue[child];
            int right = child + 1;
            if (right < size &&
                    ((Comparable<? super T>) c).compareTo((T) pQueue[right]) > 0)
                c = pQueue[child = right];
            if (key.compareTo((T) c) <= 0)
                break;
            pQueue[k] = c;
            k = child;
        }
        pQueue[k] = key;
    }


    public T peek() {
        if (index == 0) {
            return null;
        } else {
            return (T) pQueue[0];
        }
    }

    public Iterator<T> iterator() {
        return new Iter();
    }

    private class Iter implements Iterator<T> {

        int indexNow = 0;

        public boolean hasNext() {
            return indexNow < index;
        }

        public T next() {
            if (hasNext()) {
                indexNow++;
                return (T) pQueue[indexNow - 1];
            }
            return null;
        }
    }
}
