package com.khanas.LogicalTasks.Model;

import java.util.Random;

public class Door {
    private boolean isMonster;
    private int value;

    public Door() {
        this.isMonster = new Random().nextBoolean();
        randomInfo();
    }

    public final boolean isMonster() {
        return isMonster;
    }

    public final void setMonster(final boolean monster1) {
        isMonster = monster1;
    }

    public final int getValue() {
        return value;
    }

    public final void setValue(final int value1) {
        this.value = value1;
    }

    public final void randomInfo() {
        if (isMonster) {
            value = new Random().nextInt(96) + 5;
        } else {
            value = new Random().nextInt(71) + 10;
        }
    }

    public final String toString() {
        if (isMonster) {
            return "is Monster with " + value + "xp";
        } else {
            return "is Artefact with " + value + "xp";
        }
    }

}
