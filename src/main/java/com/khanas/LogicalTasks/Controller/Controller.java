package com.khanas.LogicalTasks.Controller;

import com.khanas.LogicalTasks.Model.Door;

import java.util.ArrayList;

public interface Controller {
    ArrayList<Door> door = new ArrayList<>();

    void randomDoors();

    ArrayList<String> getDoors();

    String[] deathDoors(int index);

    String winningPath();
}
