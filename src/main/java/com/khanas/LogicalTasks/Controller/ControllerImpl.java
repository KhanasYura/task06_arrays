package com.khanas.LogicalTasks.Controller;

import com.khanas.LogicalTasks.Model.Door;

import java.util.ArrayList;

public class ControllerImpl implements Controller {
    private int heroXp = 25;
    private int doors = 10;
    String[] info = new String[doors];


    public void randomDoors() {
        for (int i = 0; i < doors; i++) {
            door.add(new Door());
        }
    }

    public ArrayList<String> getDoors() {
        ArrayList<String> infoDoors = new ArrayList<>();
        for (int i = 0; i < door.size(); i++) {
            infoDoors.add("#" + (i + 1) + "door - " + door.get(i).toString());
        }
        return infoDoors;
    }

    public String[] deathDoors(int index) {
        if (index > doors - 1) {
            return info;
        } else {
            if (door.get(index).isMonster() && door.get(index).getValue() > heroXp) {
                info[index] = (index + 1 + " door - death");
            } else {
                info[index] = (index + 1 + " door - live");
            }
            return deathDoors(index + 1);
        }
    }

    public String winningPath() {
        int sumArtefact = 0;
        int sumMonster = 0;
        for (Door door2 : door) {
            if (door2.isMonster()) {
                sumMonster += door2.getValue();
            } else {
                sumArtefact += door2.getValue();
            }
        }
        if (sumArtefact - sumMonster >= 0) {
            StringBuilder sbMonster = new StringBuilder();
            StringBuilder sbArtefact = new StringBuilder();
            for (int i = 0; i < door.size(); i++) {
                if (door.get(i).isMonster()) {
                    sbMonster.append(i + 1).append(" ");
                } else {
                    sbArtefact.append(i + 1).append(" ");
                }
            }
            return ("Right way: " + sbArtefact + sbMonster);
        } else {
            return ("Hero loses");
        }
    }

}
