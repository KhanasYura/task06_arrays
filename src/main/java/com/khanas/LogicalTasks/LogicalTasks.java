package com.khanas.LogicalTasks;

import com.khanas.LogicalTasks.View.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class LogicalTasks {
    private Logger logger = LogManager.getLogger(LogicalTasks.class);
    private ArrayList<Integer> arr1 = new ArrayList<>();
    private ArrayList<Integer> arr2 = new ArrayList<>();
    private HashSet<Integer> arr3 = new HashSet<>();
    private HashSet<Integer> arr4 = new HashSet<>();
    private Random random = new Random();

    public LogicalTasks() {
        arr1 = randomArr();
        arr2 = randomArr();
        logger.info("--------------Task1--------------");
        task1();
        logger.info("--------------Task2--------------");
        task2();
        logger.info("--------------Task3--------------");
        task3();
        logger.info("---------------Game--------------");
        new MyView();
    }

    public final ArrayList<Integer> randomArr() {
        ArrayList<Integer> arr = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            arr.add(random.nextInt(5));
        }
        logger.info(arr);
        return arr;
    }


    public final void task3() {
        ArrayList<Integer> arr = new ArrayList<>();
        int count = 0;
        for (int i = 0; i < arr2.size() - 1; i++) {
            if (count == 0 && arr2.get(i) == arr2.get(i + 1)) {
                arr.add(arr2.get(i));
                count++;
            }
            if (arr2.get(i) != arr2.get(i + 1) && count == 0) {
                arr.add(arr2.get(i));
            }
            if (arr2.get(i) != arr2.get(i + 1) && count > 0) {
                count = 0;
            }
        }
        arr2.clear();
        arr2 = arr;
        logger.info(arr2);
    }

    public final void task2() {
        ArrayList<Integer> arr = new ArrayList<>();

        for (int i = 0; i < arr1.size(); i++) {
            int count = 0;
            int count2 = 0;
            for (int j = 0; j < arr1.size(); j++) {
                for (int k = 0; k < arr.size(); k++) {
                    if (arr1.get(i) == arr.get(k)) {
                        count2 = 1;
                        break;
                    }
                }
                if (arr1.get(i) == arr1.get(j)) {
                    count++;
                }
                if (count == 3 || count2 == 1) {
                    break;
                }
            }
            if (count != 3 && count2 != 1) {
                arr.add(arr1.get(i));
            }
        }
        arr1.clear();
        arr1 = arr;
        logger.info(arr1);
    }

    public final void task1() {
        for (int i = 0; i < arr1.size(); i++) {
            int check = 0;
            for (int j = 0; j < arr2.size(); j++) {
                if (arr1.get(i) == arr2.get(j)) {
                    check++;
                    break;
                }
            }
            if (check == 0) {
                arr4.add(arr1.get(i));
            } else {
                arr3.add(arr1.get(i));
            }
        }
        logger.info("arr3: " + arr3);
        logger.info("arr4: " + arr4);
    }
}
