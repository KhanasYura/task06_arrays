package com.khanas.LogicalTasks.View;

import com.khanas.LogicalTasks.Controller.Controller;
import com.khanas.LogicalTasks.Controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;
import java.util.ArrayList;

public class MyView {
    private static Logger logger = LogManager.getLogger();
    private static Scanner sc = new Scanner(System.in);
    private Controller controller;

    public MyView() {
        controller = new ControllerImpl();
        while (true) {
            logger.info("1.Fill the doors");
            logger.info("2.Show info about the door");
            logger.info("3.Show the death doors");
            logger.info("4.The right way to win");
            logger.info("5.Exit");
            logger.info("Enter: ");
            int index = sc.nextInt();
            if (index == 1) {
                controller.randomDoors();
            } else if (index == 2) {
                ArrayList<String> doors = controller.getDoors();
                for (String str : doors) {
                    logger.info(str);
                }
            } else if (index == 3) {
                String[] doors = controller.deathDoors(0);
                for (String str : doors) {
                    logger.info(str);
                }
            } else if (index == 4) {
                logger.info(controller.winningPath());
            } else if (index == 5) {
                break;
            } else {
                logger.error("Wrong index.");
            }
        }
    }
}
