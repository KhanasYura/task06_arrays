package com.khanas.Generic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;
import java.util.ArrayList;

public class ViewGeneric {
    private static Logger logger = LogManager.getLogger();
    private static Scanner sc = new Scanner(System.in);

    public ViewGeneric() {
        ArrayList<GenericTask1<String>> arr = new ArrayList<>();
        while (true) {
            logger.info("1.Add person");
            logger.info("2.Show person");
            logger.info("3.Show people");
            logger.info("4.Set name");
            logger.info("5.Set surname");
            logger.info("6.Set username");
            logger.info("7.Set password");
            logger.info("8.Exit");
            logger.info("Enter: ");
            int index = sc.nextInt();
            if (index == 1) {
                logger.info("Enter name:");
                sc.nextLine();
                String name = sc.nextLine();
                logger.info("Enter surname:");
                String surname = sc.nextLine();
                logger.info("Enter username:");
                String username = sc.nextLine();
                logger.info("Enter password:");
                String password = sc.nextLine();
                arr.add(new GenericTask1<>(name, surname, username, password));
            } else if (index == 2) {
                logger.info("Enter index:");
                int index1 = sc.nextInt();
                if (index1 < 0 || index1 > arr.size()) {
                    logger.error("Wrong answear");
                } else {
                    logger.info(arr.get(index1).getInfo());
                }
            } else if (index == 3) {
                for (int i = 0; i < arr.size(); i++) {
                    logger.info(arr.get(i).getInfo());
                }
            } else if (index == 4) {
                logger.info("Enter person:");
                int index1 = sc.nextInt();
                if (index1 < 0 || index1 > arr.size()) {
                    logger.error("Wrong answear");
                } else {
                    logger.info("Enter name:");
                    sc.nextLine();
                    String name = sc.nextLine();
                    arr.get(index1).setName(name);
                }
            } else if (index == 5) {
                logger.info("Enter person:");
                int index1 = sc.nextInt();
                if (index1 < 0 || index1 > arr.size()) {
                    logger.error("Wrong answear");
                } else {
                    logger.info("Enter surname:");
                    sc.nextLine();
                    String surname = sc.nextLine();
                    arr.get(index1).setSurname(surname);
                }
            } else if (index == 6) {
                logger.info("Enter person:");
                int index1 = sc.nextInt();
                if (index1 < 0 || index1 > arr.size()) {
                    logger.error("Wrong answear");
                } else {
                    logger.info("Enter username:");
                    sc.nextLine();
                    String username = sc.nextLine();
                    arr.get(index1).setUsername(username);
                }
            } else if (index == 7) {
                logger.info("Enter person:");
                int index1 = sc.nextInt();
                if (index1 < 0 || index1 > arr.size()) {
                    logger.error("Wrong answear");
                } else {
                    logger.info("Enter password:");
                    sc.nextLine();
                    String password = sc.nextLine();
                    arr.get(index1).setPassword(password);
                }
            } else if (index == 8) {
                break;
            } else {
                logger.error("Wrong input");
            }
        }
    }
}
