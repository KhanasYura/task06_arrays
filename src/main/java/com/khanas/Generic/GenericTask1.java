package com.khanas.Generic;

import java.util.ArrayList;

public class GenericTask1<T> {
    T name;
    T surname;
    T username;
    T password;

    public GenericTask1(T name, T surname, T username, T password) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
    }

    public T getName() {
        return name;
    }

    public void setName(T name) {
        this.name = name;
    }

    public T getSurname() {
        return surname;
    }

    public void setSurname(T surname) {
        this.surname = surname;
    }

    public T getUsername() {
        return username;
    }

    public void setUsername(T username) {
        this.username = username;
    }

    public T getPassword() {
        return password;
    }

    public void setPassword(T password) {
        this.password = password;
    }

    public ArrayList<? super T> getInfo() {
        ArrayList<? super T> info = new ArrayList<>();
        info.add(name);
        info.add(surname);
        info.add(username);
        info.add(password);
        return info;
    }
}
